package e.pnikrewicz.test3.login.services

import e.pnikrewicz.test3.login.utils.OperationResult
import e.pnikrewicz.test3.login.utils.ValidationResult
import io.reactivex.Observable

class LoginUseCase {
    private val retroService by lazy { RetroRequestService() }
    private val validator by lazy { LoginInputsValidator() }

    fun login(credentials: e.pnikrewicz.test3.login.utils.Credentials): Observable<OperationResult>{
        return retroService.login(credentials)
    }

    fun validateUsername(username: String): Observable<ValidationResult> {
        return validator.validateUsername(username)
    }

    fun validatePassword(password: String): Observable<ValidationResult>{
        return validator.validatePassword(password)
    }
}