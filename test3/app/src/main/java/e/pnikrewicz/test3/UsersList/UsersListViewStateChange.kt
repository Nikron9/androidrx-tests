package e.pnikrewicz.test3.UsersList

sealed class UsersListViewStateChange {
    data class UsersListChanged(val refreshingResult: UsersListRefreshingResult) : UsersListViewStateChange()
}