package e.pnikrewicz.test3

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import e.pnikrewicz.test3.login.utils.Credentials
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST



interface RetroService {
    @POST("api/login")
    fun login(@Header("Content-Type") contenttype: String = "application/json",
              @Body credentials: Credentials): Observable<JsonObject>

    @GET("api/usersmanagement/getallusers/")
    fun getAllUsers(@Header("Content-Type") contenttype: String = "application/json",
                    @Header("Authorization") token:String) : Observable<JsonArray>
}

