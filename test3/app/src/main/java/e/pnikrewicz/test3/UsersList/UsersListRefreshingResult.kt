package e.pnikrewicz.test3.UsersList

import e.pnikrewicz.test3.models.DatabaseUser

sealed class UsersListRefreshingResult {
    class Pending : UsersListRefreshingResult()
    data class Completed(val usersList: ArrayList<DatabaseUser>) : UsersListRefreshingResult()
    data class Error(val error: String) : UsersListRefreshingResult()
}