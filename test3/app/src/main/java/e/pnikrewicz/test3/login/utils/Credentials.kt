package e.pnikrewicz.test3.login.utils

data class Credentials(val email:String, val password:String)