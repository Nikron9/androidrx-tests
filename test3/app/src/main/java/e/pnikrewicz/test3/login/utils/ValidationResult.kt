package e.pnikrewicz.test3.login.utils

data class ValidationResult(val isValid: Boolean, val message: String)