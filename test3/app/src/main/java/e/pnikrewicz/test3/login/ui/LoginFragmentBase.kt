package e.pnikrewicz.test3.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.mosby3.mvi.MviFragment
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import e.pnikrewicz.test3.R
import e.pnikrewicz.test3.login.mvi.LoginPresenter
import e.pnikrewicz.test3.login.mvi.LoginView
import e.pnikrewicz.test3.login.utils.Credentials
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_main.*

abstract class LoginFragmentBase : MviFragment<LoginView, LoginPresenter>(), LoginView {
    override fun createPresenter(): LoginPresenter = LoginPresenter()

    override val usernameIntent: Observable<String>
        get() = txtUsername.textChanges().map { it.toString() }

    override val passwordIntent: Observable<String>
        get() = txtPassword.textChanges().map { it.toString() }

    override val loginIntent: Observable<Credentials>
        get() = btnLogin.clicks()
                .map {
                    Credentials(txtUsername.text.toString(), txtPassword.text.toString())
                }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }
}