package e.pnikrewicz.test3.login.services

import e.pnikrewicz.test3.login.utils.ValidationResult
import io.reactivex.Observable

class LoginInputsValidator{
    fun validateUsername(username: String): Observable<ValidationResult> {
        if (username.isEmpty()){
            return Observable.just(ValidationResult(false, "Username cannot be empty"))
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches()){
            return Observable.just(ValidationResult(false, "Email is not valid"))
        }
        return Observable.just(ValidationResult(true, ""))
    }

    fun validatePassword(password: String): Observable<ValidationResult> {
        return Observable.just(ValidationResult(password.isNotEmpty(), "Password cannot be empty"))
    }
}

