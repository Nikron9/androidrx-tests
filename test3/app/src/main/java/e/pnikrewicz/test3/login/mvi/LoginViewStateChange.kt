package e.pnikrewicz.test3.login.mvi

import e.pnikrewicz.test3.login.utils.OperationResult
import e.pnikrewicz.test3.login.utils.ValidationResult

sealed class LoginViewStateChange {
    data class UsernameChanged(val validationResult: ValidationResult) : LoginViewStateChange()
    data class PasswordChanged(val validationResult: ValidationResult) : LoginViewStateChange()
    data class LoginChanged(val operationResult: OperationResult) : LoginViewStateChange()
}