package e.pnikrewicz.test3.login.services

import e.pnikrewicz.test3.RetroService
import e.pnikrewicz.test3.login.utils.Credentials
import e.pnikrewicz.test3.login.utils.OperationResult
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetroRequestService {
    var retrofit = Retrofit.Builder()
            .baseUrl("http://tournamentmanagementapi.azurewebsites.net/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    var service = retrofit.create<RetroService>(RetroService::class.java)

    fun login(credentials: Credentials) : Observable<OperationResult> {
        return service.login("application/json", credentials)
                .map{
                    if (it["token"] != null) {
                        OperationResult.Completed(it["token"].toString())
                    } else {
                        OperationResult.Error(it["message"].toString())
                    }
                }
                .startWith(OperationResult.Pending())
    }
}