package e.pnikrewicz.test3.UsersList

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import e.pnikrewicz.test3.RetroService
import e.pnikrewicz.test3.models.DatabaseUser
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class UsersListPresenter(private val sharedPrefs: SharedPreferences) : MviBasePresenter<UsersListView, UsersListViewState>() {

    val reducer = UsersListViewReducer()

    var retrofit = Retrofit.Builder()
            .baseUrl("http://tournamentmanagementapi.azurewebsites.net/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    var service = retrofit.create<RetroService>(RetroService::class.java)

    var listType = object : TypeToken<ArrayList<DatabaseUser>>() {

    }.type

    override fun bindIntents() {
        val refreshIntent = intent { it.refreshIntent }
                .observeOn(Schedulers.io())
                .switchMap {
                    val token = sharedPrefs.getString("token","")
                    service.getAllUsers("application/json", "Bearer $token")
                            .map {

                                    UsersListRefreshingResult.Completed(Gson().fromJson(it, listType)) as UsersListRefreshingResult

                            }
                            .startWith(UsersListRefreshingResult.Pending())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .map { UsersListViewStateChange.UsersListChanged(it) as UsersListViewStateChange }
                .scan(UsersListViewState()) { state: UsersListViewState, change: UsersListViewStateChange ->
                    return@scan reducer.reduce(state, change)
                }

        subscribeViewState(refreshIntent, { view, viewState ->
            view.render(viewState)
        })
    }
}