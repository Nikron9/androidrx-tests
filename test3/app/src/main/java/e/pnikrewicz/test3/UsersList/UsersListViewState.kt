package e.pnikrewicz.test3.UsersList

import e.pnikrewicz.test3.models.DatabaseUser

data class UsersListViewState (
    var isRefreshingPending: Boolean = false,
    var isRefreshingCompleted: Boolean = false,
    var isRefreshingFailed: Boolean = false,
    var usersList: ArrayList<DatabaseUser> = ArrayList(),
    var message:String = ""
)
