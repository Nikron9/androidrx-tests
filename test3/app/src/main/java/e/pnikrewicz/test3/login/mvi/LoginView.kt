package e.pnikrewicz.test3.login.mvi

import com.hannesdorfmann.mosby3.mvp.MvpView
import e.pnikrewicz.test3.login.utils.Credentials
import io.reactivex.Observable


interface LoginView : MvpView {
    val usernameIntent: Observable<String>
    val passwordIntent: Observable<String>
    val loginIntent: Observable<Credentials>

    fun render(state: LoginViewState)
}

