package e.pnikrewicz.test3.login.mvi

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import e.pnikrewicz.test3.login.services.LoginUseCase
import e.pnikrewicz.test3.login.services.LoginInputsValidator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginPresenter : MviBasePresenter<LoginView, LoginViewState>() {

    private val reducer = LoginViewStateReducer()
    private val useCase by lazy { LoginUseCase() }

    override fun bindIntents() {
        val passwordIntent = intent { it.passwordIntent }
                .switchMap { useCase.validatePassword(it) }
                .map { LoginViewStateChange.PasswordChanged(it) }

        val usernameIntent = intent { it.usernameIntent }
                .switchMap { useCase.validateUsername(it) }
                .map { LoginViewStateChange.UsernameChanged(it) }

        val loginIntent = intent { it.loginIntent }
                .observeOn(Schedulers.io())
                .switchMap {
                    useCase.login(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .map { LoginViewStateChange.LoginChanged(it) as LoginViewStateChange }

        val stream = Observable
                .merge(passwordIntent, loginIntent, usernameIntent)
                .scan(LoginViewState()) { state: LoginViewState, change: LoginViewStateChange ->
                    return@scan reducer.reduce(state,change)
                }

        subscribeViewState(stream) { view, viewState ->
            view.render(viewState)
        }
    }
}