package e.pnikrewicz.test3.login.utils

sealed class OperationResult {
    class NotStarted : OperationResult()
    class Pending : OperationResult()
    data class Completed(val token: String) : OperationResult()
    data class Error(val error: String) : OperationResult()
}