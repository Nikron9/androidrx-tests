package e.pnikrewicz.test3.login.mvi

data class LoginViewState(
        var isUsernameValid: Boolean = false,
        var isPasswordValid: Boolean = false,
        var isLoginPending: Boolean = false,
        var isLoginCompleted: Boolean = false,
        var isLoginFailed: Boolean = false,
        var message:String = ""
)