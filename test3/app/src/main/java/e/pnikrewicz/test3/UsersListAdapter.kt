package e.pnikrewicz.test3

import android.provider.ContactsContract
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import e.pnikrewicz.test3.models.DatabaseUser

class UsersListAdapter : RecyclerView.Adapter<UserListViewHolder>() {


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    var myDataset = ArrayList<DatabaseUser>()

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int):UserListViewHolder {
        var cardItem = LayoutInflater.from(parent.context).inflate(R.layout.user_holder, parent, false)
        return UserListViewHolder(cardItem)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: UserListViewHolder, position: Int) {
        var databaseUser = myDataset[position]
        holder.UpdateWithDatabaseUser(databaseUser)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}