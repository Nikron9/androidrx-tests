package e.pnikrewicz.test3.UsersList

import android.util.Log

class UsersListViewReducer{
    fun reduce(state: UsersListViewState, change: UsersListViewStateChange): UsersListViewState {
        val currentState = state.copy()
        Log.i("changes", change.toString())
        when (change) {
            is UsersListViewStateChange.UsersListChanged -> {
                when (change.refreshingResult){
                    is UsersListRefreshingResult.Pending -> {
                        currentState.isRefreshingPending = true
                        currentState.isRefreshingCompleted = false
                        currentState.isRefreshingFailed = false
                    }
                    is UsersListRefreshingResult.Completed -> {
                        currentState.isRefreshingPending = false
                        currentState.isRefreshingCompleted = true
                        currentState.isRefreshingFailed = false
                        currentState.usersList = change.refreshingResult.usersList
                    }
                    is UsersListRefreshingResult.Error -> {
                        currentState.isRefreshingPending = false
                        currentState.isRefreshingCompleted = false
                        currentState.isRefreshingFailed = true
                    }
                }
            }
        }
        return currentState
    }
}