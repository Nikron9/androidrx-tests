package e.pnikrewicz.test3


import android.content.Context
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.mosby3.mvi.MviFragment
import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.view.drags
import e.pnikrewicz.test3.UsersList.UsersListPresenter
import e.pnikrewicz.test3.UsersList.UsersListView
import e.pnikrewicz.test3.UsersList.UsersListViewState
import e.pnikrewicz.test3.models.DatabaseUser
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_users_list.*

class UsersListActivityFragment : MviFragment<UsersListView, UsersListPresenter>(), UsersListView {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: UsersListAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun createPresenter(): UsersListPresenter = UsersListPresenter(activity!!.getSharedPreferences("TMApi", Context.MODE_PRIVATE))

    override lateinit var refreshIntent: Observable<ArrayList<DatabaseUser>>

    override fun render(state: UsersListViewState) {
        listRefresher.isRefreshing = state.isRefreshingPending

        if (state.isRefreshingCompleted) {

            viewAdapter.myDataset.clear()
            viewAdapter.myDataset = state.usersList
            viewAdapter.notifyDataSetChanged()
            listRefresher.isRefreshing = false

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_users_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewManager = LinearLayoutManager(this.context)
        viewAdapter = UsersListAdapter()

        recyclerView = this.view!!.findViewById<RecyclerView>(R.id.usersListRecyclerView).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
        refreshIntent = RxSwipeRefreshLayout.refreshes(listRefresher)
                .map {
                    return@map ArrayList<DatabaseUser>()
                }
    }


}
