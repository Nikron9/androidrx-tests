package e.pnikrewicz.test3.login.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.hannesdorfmann.mosby3.mvi.MviFragment
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import e.pnikrewicz.test3.R
import e.pnikrewicz.test3.UsersListActivity
import e.pnikrewicz.test3.login.mvi.LoginPresenter
import e.pnikrewicz.test3.login.mvi.LoginView
import e.pnikrewicz.test3.login.mvi.LoginViewState
import e.pnikrewicz.test3.login.utils.Credentials
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_main.*

class LoginFragment : LoginFragmentBase() {

    override fun render(state: LoginViewState) {

        if (state.isUsernameValid.or(txtPassword.isFocused).or(mainLayout.isFocused)){
            txtUsernameLayout.error = null
        }
        else{
            txtUsernameLayout.error = state.message
        }

        if (state.isPasswordValid.or(txtUsername.isFocused.or(mainLayout.isFocused))){
            txtPasswordLayout.error = null
        }
        else{
            txtPasswordLayout.error = state.message
        }

        btnLogin.isEnabled = state.isPasswordValid && state.isUsernameValid

        if (state.isLoginCompleted){
            val prefs: SharedPreferences = activity!!.getSharedPreferences("TMApi", Context.MODE_PRIVATE)
            val edit: SharedPreferences.Editor
            val token = state.message.replace("\"","")
            edit = prefs.edit()
            edit.putString("token", token)
            edit.apply()

            val text = state.message
            val duration = Toast.LENGTH_LONG

            val toast = Toast.makeText(context, text, duration)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
            state.isLoginCompleted = true

            startActivity(Intent(this.context, UsersListActivity::class.java))

            this.activity!!.finish()

        }

        if (state.isLoginPending){

            progressBar.visibility = View.VISIBLE
        }
        else{
            progressBar.visibility = View.GONE
        }

        if(state.isLoginFailed){
            val text = state.message
            val duration = Toast.LENGTH_LONG

            val toast = Toast.makeText(context, text, duration)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
            state.isLoginFailed = false
        }
    }


}
