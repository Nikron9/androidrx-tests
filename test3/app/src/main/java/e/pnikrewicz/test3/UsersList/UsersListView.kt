package e.pnikrewicz.test3.UsersList

import com.hannesdorfmann.mosby3.mvp.MvpView
import e.pnikrewicz.test3.models.DatabaseUser
import io.reactivex.Observable


interface UsersListView : MvpView {
    val refreshIntent: Observable<ArrayList<DatabaseUser>>

    fun render(state: UsersListViewState)
}