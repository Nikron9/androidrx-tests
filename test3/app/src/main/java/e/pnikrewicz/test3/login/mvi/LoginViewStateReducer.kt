package e.pnikrewicz.test3.login.mvi

import android.util.Log
import e.pnikrewicz.test3.login.utils.OperationResult

class LoginViewStateReducer{
    fun reduce(state: LoginViewState, change: LoginViewStateChange): LoginViewState {
        val currentState = state.copy()
        Log.i("changes", change.toString())
        when (change) {

            is LoginViewStateChange.UsernameChanged -> {
                currentState.isUsernameValid = change.validationResult.isValid
                currentState.message = change.validationResult.message
            }
            is LoginViewStateChange.PasswordChanged -> {
                currentState.isPasswordValid = change.validationResult.isValid
                currentState.message = change.validationResult.message
            }
            is LoginViewStateChange.LoginChanged -> {
                when (change.operationResult) {
                    is OperationResult.NotStarted -> {
                        currentState.isLoginPending = false
                        currentState.isLoginCompleted = false
                        currentState.isLoginFailed = false
                    }
                    is OperationResult.Pending -> {
                        currentState.isLoginPending = true
                        currentState.isLoginCompleted = false
                        currentState.isLoginFailed = false
                    }
                    is OperationResult.Completed -> {
                        currentState.isLoginCompleted = true
                        currentState.isLoginPending = false
                        currentState.isLoginFailed = false
                        currentState.message = change.operationResult.token
                    }
                    is OperationResult.Error -> {
                        currentState.isLoginCompleted = false
                        currentState.isLoginPending = false
                        currentState.isLoginFailed = true
                        currentState.message = change.operationResult.error
                    }
                }

            }
        }
        return currentState
    }
}