package e.pnikrewicz.test2.fragments


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.jakewharton.rxbinding2.view.clicks

import e.pnikrewicz.test2.R
import e.pnikrewicz.test2.ViewStateChange
import e.pnikrewicz.test2.activities.UsersListActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import khttp.post
import kotlinx.android.synthetic.main.fragment_login_inputs.*

class LoginInputsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.fragment_login_inputs, container, false)


        // Inflate the layout for this fragment
        return inf
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSubmit
                .clicks().subscribe {
                    getTokenFromApi()
                }
    }

    private fun getToken(token: String): Observable<ViewStateChange> {
        return Observable
                .just(token)
                .map {
                    ViewStateChange.Completed(it) as ViewStateChange
                }
                .subscribeOn(Schedulers.io())
    }

    private fun apiCall(): String {
        return post(url = "http://tournamentmanagementapi.azurewebsites.net/api/login", json = mapOf("Password" to "${passwordInput.text}", "Email" to "${loginInput.text}"))
                .jsonObject["token"]
                .toString()
    }

    fun getTokenFromApi() {
        val prefs = this.activity!!.getSharedPreferences("TMApi", Context.MODE_PRIVATE)

        val edit: SharedPreferences.Editor
        edit = prefs.edit()

        Observable.just("TOKEN")
                .subscribeOn(Schedulers.io())
                .switchMap {
                    getToken(apiCall())
                }
                .startWith(ViewStateChange.InProgress() as ViewStateChange)
                .onErrorReturn {
                    ViewStateChange.Error(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { it ->
                    when (it) {
                        is ViewStateChange.InProgress -> {
                            progressBar.visibility = View.VISIBLE
                        }
                        is ViewStateChange.Error -> {
                            resultText.text = it.error.message
                            progressBar.visibility = View.GONE
                        }
                        is ViewStateChange.Completed -> {
                            resultText.text = it.text
                            progressBar.visibility = View.GONE

                            edit.putString("token", "${resultText.text}")
                            edit.apply()

                            val intent = Intent(activity, UsersListActivity::class.java)
                            activity!!.startActivity(intent)
                        }
                    }
                }
    }
}
