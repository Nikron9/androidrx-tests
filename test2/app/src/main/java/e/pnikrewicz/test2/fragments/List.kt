package e.pnikrewicz.test2.fragments


import android.content.Context
import android.os.Bundle
import android.support.annotation.MainThread
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import e.pnikrewicz.test2.R
import e.pnikrewicz.test2.models.UserModel
import khttp.get
import kotlin.collections.List
import com.google.gson.reflect.TypeToken
import java.util.*
import java.util.Arrays.asList
import java.util.Arrays.asList
import java.util.Arrays.asList








class List : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val prefs = this.activity?.getSharedPreferences("TMApi", Context.MODE_PRIVATE)


        getActualUserList(prefs?.getString("token","")!!)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    private fun getActualUserList(accessToken: String): List<UserModel> {
        val response = get("http://tournamentmanagementapi.azurewebsites.net/api/usersmanagement/getallusers/", headers = mapOf("Authorization" to "Bearer $accessToken"))
        val jsonResponse = response.jsonArray
        val listToDeserialize = jsonResponse.toString()

        val usersArray = Gson().fromJson(listToDeserialize, Array<UserModel>::class.java)

        return usersArray.toList()
    }
}
