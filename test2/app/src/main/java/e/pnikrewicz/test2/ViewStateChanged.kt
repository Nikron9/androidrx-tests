package e.pnikrewicz.test2

sealed class ViewStateChange {
    class InProgress : ViewStateChange()
    data class Error(val error:Throwable) : ViewStateChange()
    data class Completed(val text:String) : ViewStateChange()



}

