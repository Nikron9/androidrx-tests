package e.pnikrewicz.test2.models

class UserModel(Id:String?,Password:String?,Email:String? ) {
    var id: String? = null
    var password: String? = null
    var email: String? = null
    var avatarPath: String? = null
    var userType: Long = 0
    var gameProfiles: Array<Any>? = null
    init {
        id = Id
        password = Password
        email = Email
    }
}